package com.five.mapper;

import com.five.domain.City;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.core.parameters.P;

import java.util.List;

@Mapper
public interface CityMapper {

    //查询城市信息
    List<City> queryAllCity(City city);

    //查询城市(App)
    City queryCity(City city);

    //导航栏查询
    List<City> queryCityByUse(City city);

    //根据name查询
    City queryCityByName(@Param("name") String name);

    //添加城市
    void alter();
    int addCity(City city);

    //修改城市信息
    int updateCity(City city);

    //删除城市信息
    int deleteCity(City city);

}
