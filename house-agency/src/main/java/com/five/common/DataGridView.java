package com.five.common;

public class DataGridView {
    private Integer code = 0;
    private String msg = "";
    private Long count = 0L;
    private Object data;

    public static final DataGridView ADD_SUCCESS = new DataGridView(200,"添加成功");
    public static final DataGridView ADD_ERROR2 = new DataGridView(500,"用户名重复");
    public static final DataGridView ADDERROR3 = new DataGridView(500,"本中介暂未有该城市的门店");
    public static final DataGridView ADD_ERROR = new DataGridView(500,"添加失败");
    public static final DataGridView REGISTER_SUCCESS = new DataGridView(200,"注册成功");
    public static final DataGridView REGISTER_ERROR = new DataGridView(500,"注册失败");
    public static final DataGridView REGISTER_ERROR2 = new DataGridView(500,"用户名和密码长度不能少于6位");
    public static final DataGridView REGISTER_ERROR3 = new DataGridView(500,"手机号码和身份证格式错误");
    public static final DataGridView UPDATE_SUCCESS = new DataGridView(200,"修改成功");
    public static final DataGridView UPDATE_PWD_ERROR = new DataGridView(500,"旧密码错误");
    public static final DataGridView UPDATE_PWD_ERROR2 = new DataGridView(500,"密码长度小于6位");
    public static final DataGridView UPDATE_ERROR = new DataGridView(500,"修改失败");
    public static final DataGridView DELETE_SUCCESS = new DataGridView(200,"删除成功");
    public static final DataGridView DELETE_ERROR = new DataGridView(500,"删除失败");
    public static final DataGridView DISPATCH_SUCCESS = new DataGridView(200,"分配成功");
    public static final DataGridView DISPATCH_ERROR = new DataGridView(500,"分配失败");
    public static final DataGridView BAN_SUCCESS = new DataGridView(200,"禁用成功");
    public static final DataGridView BAN_ERROR = new DataGridView(500,"禁用失败");
    public static final DataGridView RESET_SUCCESS = new DataGridView(200,"重置成功");
    public static final DataGridView RESET_ERROR = new DataGridView(500,"重置失败");
    public static final DataGridView UPLOAD_SUCCESS = new DataGridView(200,"上传成功");
    public static final DataGridView UPLOAD_ERROR = new DataGridView(500,"上传失败");
    public static final DataGridView LOOK_SUCCESS = new DataGridView(200,"查看成功");
    public static final DataGridView LOOK_ERROR = new DataGridView(500,"查看失败");
    public static final DataGridView CANCEL_SUCCESS = new DataGridView(200,"取消成功");
    public static final DataGridView CANCEL_ERROR = new DataGridView(500,"取消失败");
    public static final DataGridView BUY_ERROR = new DataGridView(500,"购买/租凭失败");
    public static final DataGridView BUY_ERROR2 = new DataGridView(501,"不能购买自己的房屋");
    public static final DataGridView PAY_SUCCESS = new DataGridView(200,"支付成功");
    public static final DataGridView PAY_ERROR = new DataGridView(500,"取消失败");

    public DataGridView(Integer code, String msg, Object data) {
        super();
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public DataGridView(Long count, Object data) {
        super();
        this.count = count;
        this.data = data;
    }

    public DataGridView(Object data) {
        super();
        this.data = data;
    }

    public DataGridView(Integer code, String msg) {
        super();
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public DataGridView() {
    }

    public DataGridView(Integer code, String msg, Long count, Object data) {
        this.code = code;
        this.msg = msg;
        this.count = count;
        this.data = data;
    }
}
