package com.five.controller.customer;

import com.five.common.DataGridView;
import com.five.common.UserUtil;
import com.five.domain.City;
import com.five.domain.House;
import com.five.service.CityService;
import com.five.service.HouseService;
import com.five.vo.CityVo;
import com.five.vo.HouseVo;
import com.five.vo.UserVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user/house")
public class HouseAppController {

    @Autowired
    HouseService houseService;
    @Autowired
    CityService cityService;

    //查询房屋(App)
    @RequestMapping("/allHouse")
    public DataGridView queryHouse(HouseVo houseVo){
        Page<Object> page = PageHelper.startPage(houseVo.getPage(), houseVo.getLimit());
        List<House> data = houseService.queryHouse(houseVo);
        return new DataGridView(page.getTotal(), data);
    }

    //查询房屋参数(App与后端共用)
    @RequestMapping("/queryHouseId")
    public DataGridView queryHouseById(HouseVo houseVo){
        House data = houseService.queryHouseById(houseVo);
        return new DataGridView(data);
    }

    //查询城市(App)
    @RequestMapping("/searchCity")
    public DataGridView searchHouse(CityVo cityVo){
        City data = cityService.queryCityByName(cityVo);
        if(data == null){
            return new DataGridView(500, "查询失败");
        }else{
            return new DataGridView(200, "查询成功", data);
        }

    }

    //最新房屋(App)
    @RequestMapping("/queryHouseByNew")
    public DataGridView queryHouseByNew(HouseVo houseVo){
        Page<Object> page = PageHelper.startPage(houseVo.getPage(), houseVo.getLimit());
        List<House> data = houseService.queryHouseByNew(houseVo);
        return new DataGridView(page.getTotal(), data);
    }

    //我的房源(App)
    @RequestMapping("/myHouse")
    public DataGridView myHouse(HouseVo houseVo){
        houseVo.setSaId(UserUtil.getUser().getId());
        List<House> data = houseService.queryAllHouse(houseVo);
        return new DataGridView(200,"", data);
    }
}
