package com.five.controller;

import com.five.common.DataGridView;
import com.five.domain.House;
import com.five.service.HouseService;
import com.five.vo.HouseVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/admin/house")
public class HouseController {

    @Autowired
    HouseService houseService;

    //查询新房
    @RequestMapping("/allNewHouse")
    @PreAuthorize("hasPermission('/admin/newHouse/toNewHouseManager',null)")
    public DataGridView listHouse(HouseVo houseVo){
        houseVo.setType(1);
        Page<Object> page = PageHelper.startPage(houseVo.getPage(), houseVo.getLimit());
        List<House> data = houseService.queryAllHouse(houseVo);
        return new DataGridView(page.getTotal(), data);
    }

    //查询二手房
    @RequestMapping("/secondHouse")
    @PreAuthorize("hasPermission('/admin/newHouse/toSecondHouseManager',null)")
    public DataGridView secondHouse(HouseVo houseVo){
        houseVo.setType(2);
        Page<Object> page = PageHelper.startPage(houseVo.getPage(), houseVo.getLimit());
        List<House> data = houseService.queryAllHouse(houseVo);
        return new DataGridView(page.getTotal(), data);
    }

    //查询租房
    @RequestMapping("/rentHouse")
    @PreAuthorize("hasPermission('/admin/newHouse/toRentManager',null)")
    public DataGridView rentHouse(HouseVo houseVo){
        houseVo.setType(3);
        Page<Object> page = PageHelper.startPage(houseVo.getPage(), houseVo.getLimit());
        List<House> data = houseService.queryAllHouse(houseVo);
        return new DataGridView(page.getTotal(), data);
    }

    //添加房屋
    @RequestMapping("/addHouse")
    @PreAuthorize("hasPermission('/admin/newHouse/addHouse','house:add')")
    public DataGridView addHouse(HouseVo houseVo){
        return houseService.addHouse(houseVo);
    }

    //修改房屋信息
    @RequestMapping("/updateHouse")
    @PreAuthorize("hasPermission('/admin/newHouse/updateHouse','house:update')")
    public DataGridView updateHouse(HouseVo houseVo){
        return houseService.updateHouse(houseVo);
    }

    //取消房屋
    @RequestMapping("/cancelHouse")
    @PreAuthorize("hasPermission('/admin/newHouse/cancelHouse','house:cancel')")
    public DataGridView cancelHouse(HouseVo houseVo){
        return houseService.cancelHouse(houseVo);
    }

    //加载房屋信息到页面
    @RequestMapping("/look")
    @PreAuthorize("hasPermission('/admin/order/toOrder',null)")
    public DataGridView lookHouse(HouseVo houseVo){
        House data = houseService.queryHouseById(houseVo);
        return new DataGridView(data);
    }
}
