package com.five.controller;

import com.five.common.DataGridView;
import com.five.domain.News;
import com.five.service.NewsService;
import com.five.vo.NewsVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/admin/news")
public class NewsController {

    @Autowired
    private NewsService newsService;

    // 查询新闻信息
    @RequestMapping("/allNews")
    @PreAuthorize("hasPermission('/admin/news/toNews',null)")
    public DataGridView listNews(NewsVo newsVo){
        Page<Object> page = PageHelper.startPage(newsVo.getPage(), newsVo.getLimit());
        List<News> data = newsService.queryAllNews(newsVo);
        return new DataGridView(page.getTotal(), data);
    }


    //添加新闻
    @RequestMapping("/addNews")
    @PreAuthorize("hasPermission('/admin/news/addNews','new:add')")
    public DataGridView addNews(NewsVo newsVo){
        return newsService.addNews(newsVo);
    }

    //修改新闻
    @RequestMapping("/updateNews")
    @PreAuthorize("hasPermission('/admin/news/updateNews','new:update')")
    public DataGridView updateNews(NewsVo newsVo){
        return newsService.updateNews(newsVo);
    }

    //删除(恢复)新闻
    @RequestMapping("/deleteNews")
    @PreAuthorize("hasPermission('/admin/news/deleteNews','new:delete')")
    public DataGridView deleteNews(NewsVo newsVo){
        return newsService.deleteNews(newsVo);
    }
}
