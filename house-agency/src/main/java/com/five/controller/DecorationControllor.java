package com.five.controller;

import com.five.common.DataGridView;
import com.five.domain.Decoration;
import com.five.service.DecorationService;
import com.five.vo.DecorationVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/admin/decoration")
public class DecorationControllor {
    @Autowired
    private DecorationService decorationService;

    //查询装修风格信息
    @RequestMapping("/allDecoration")
    @PreAuthorize("hasPermission('/admin/decoration/toDecoration',null)")
    public DataGridView listDecoration(DecorationVo decorationVo){
        Page<Object> page = PageHelper.startPage(decorationVo.getPage(), decorationVo.getLimit());
        List<Decoration> data = decorationService.queryAllDecoration(decorationVo);
        return new DataGridView(page.getTotal(), data);
    }

    //可用装修风格
    @RequestMapping("/getDecoration1")
    @PreAuthorize("hasPermission('/admin',null)")

    public DataGridView getDecoration1(DecorationVo decorationVo){
        decorationVo.setAvailable(1);
        List<Decoration> data = decorationService.queryDecorationByUse(decorationVo);
        return new DataGridView(data);
    }

    //查询导航栏
    @RequestMapping("/getDecoration2")
    @PreAuthorize("hasPermission('/admin',null)")
    public DataGridView getDecoration2(DecorationVo decorationVo){
        List<Decoration> data = decorationService.queryDecorationByUse(decorationVo);
        return new DataGridView(data);
    }

    //添加装修风格
    @RequestMapping("/addDecoration")
    @PreAuthorize("hasPermission('/admin/decoration/addDecoration','decoration:add')")
    public DataGridView addDecoration(DecorationVo decorationVo){
        return decorationService.addDecoration(decorationVo);
    }

    //修改装修风格
    @RequestMapping("/updateDecoration")
    @PreAuthorize("hasPermission('/admin/decoration/updateDecoration','decoration:update')")
    public DataGridView updateDecoration(DecorationVo decorationVo){
        return decorationService.updateDecoration(decorationVo);
    }

    //删除(恢复)装修风格信息
    @RequestMapping("/deleteDecoration")
    @PreAuthorize("hasPermission('/admin/decoration/deleteDecoration','decoration:delete')")
    public DataGridView deleteDecoration(DecorationVo decorationVo){
        return decorationService.deleteDecoration(decorationVo);
    }
}
