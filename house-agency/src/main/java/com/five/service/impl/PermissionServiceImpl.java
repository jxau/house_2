package com.five.service.impl;

import com.five.common.DataGridView;
import com.five.common.TreeNode;
import com.five.common.TreeNodeBuilder;
import com.five.common.UserUtil;
import com.five.config.security.User;
import com.five.domain.SysPermission;
import com.five.mapper.PermissionMapper;
import com.five.service.PermissionService;
import com.five.vo.PermissionVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class PermissionServiceImpl implements PermissionService {
    @Autowired
    private PermissionMapper permissionMapper;

    //查询所有菜单
    @Override
    public List<SysPermission> queryAllPermission(PermissionVo permissionVo) {
        return permissionMapper.queryAllPermission(permissionVo);
    }

    //左侧导航栏业务处理
    @Override
    public List<TreeNode> loadIndexLeftPermissionTreeJson(PermissionVo permissionVo) {
        User user = UserUtil.getUser();
        List<SysPermission> list = permissionMapper.queryPermissionByUserId(1,user.getId(),permissionVo.getType());

        List<TreeNode> nodes = new ArrayList<>();
        for (SysPermission p : list) {
            nodes.add(new TreeNode(p.getId(), p.getPid(), p.getTitle(), p.getIcon(), p.getHref(), p.getOpen() == 1 ? true : false, p.getTarget()));
        }
        List<TreeNode> treeNodes = TreeNodeBuilder.build(nodes, 1);
        return treeNodes;
    }

    //菜单页面左侧业务处理
    @Override
    public DataGridView loadLeftManagerPermissionTreeJson(PermissionVo permissionVo) {
        permissionVo.setAvailable(1);//只查询可用的
        List<SysPermission> list = permissionMapper.queryAllPermission(permissionVo);
        List<TreeNode> nodes = new ArrayList<>();
        //把list里面的数据放到nodes
        for (SysPermission p : list) {
            nodes.add(new TreeNode(p.getId(), p.getPid(), p.getTitle(), p.getIcon(), p.getHref(), p.getOpen() == 1 ? true : false, p.getTarget()));

        }
        return new DataGridView(nodes);
    }

    //添加权限或菜单
    @Override
    @Transactional
    public void addPermission(PermissionVo permissionVo) {
        permissionMapper.addPermission(permissionVo);
    }

    //修改权限或菜单
    @Override
    @Transactional
    public void updatePermission(PermissionVo permissionVo) {
        if (permissionVo.getAvailable() == 0) {
            PermissionVo p = new PermissionVo();
            p.setPid(permissionVo.getId());
            List<SysPermission> list = permissionMapper.queryAllPermission2(p);
            for (SysPermission permission : list) {
                PermissionVo p2 = new PermissionVo();
                p2.setPid(permission.getId());
                List<SysPermission> list2 = permissionMapper.queryAllPermission2(p2);
                if (list.size() > 0) {
                    for (SysPermission permission2 : list2) {
                        permission2.setAvailable(0);
                        permissionMapper.updatePermission(permission2);
                    }
                }
                permission.setAvailable(0);
                permissionMapper.updatePermission(permission);
            }
        }
        if (permissionVo.getAvailable() == 1) {
            PermissionVo p = new PermissionVo();
            p.setId(permissionVo.getPid());
            List<SysPermission> list = permissionMapper.queryAllPermission2(p);
            for (SysPermission permission : list) {
                PermissionVo p2 = new PermissionVo();
                p2.setId(permission.getPid());
                List<SysPermission> list2 = permissionMapper.queryAllPermission2(p2);
                if (list.size() > 0) {
                    for (SysPermission permission2 : list2) {
                        permission2.setAvailable(1);
                        permissionMapper.updatePermission(permission2);
                    }
                }
                permission.setAvailable(1);
                permissionMapper.updatePermission(permission);
            }
        }
        permissionMapper.updatePermission(permissionVo);
    }

    //删除权限或菜单
    @Override
    @Transactional
    public void deletePermission(Integer id) {
        permissionMapper.deletePermission(id);
        permissionMapper.deleteRolePermissionPid(id);
    }

    //根据角色id查询权限或菜单
    @Override
    public List<SysPermission> queryPermissionByRoleId(Integer available,Integer id){
        return permissionMapper.queryPermissionByRoleId(available,id);
    }
}
