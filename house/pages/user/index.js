import {request} from "../../request/index.js";

Page({
  data: {
    
  },
  onLoad: function (options) {
    if(wx.getStorageSync("cookieKey")==null||wx.getStorageSync("cookieKey")==""){
      wx.redirectTo({
        url: '../login/index',
      });
      return;
    }
    this.LoadUser();
  },
  //加载用户信息
  async LoadUser(){
    //创建header
    let header;
    header = {
      'content-type': 'application/x-www-form-urlencoded',
      'cookie':wx.getStorageSync("cookieKey")//读取cookie
    };
    const res = await request({ url: "/user/customer/userinfo", method: "POST",header: header });
    if(res.data.code===200){
      if (res && res.header && res.header['Set-Cookie']) {
        wx.setStorageSync('cookieKey', res.header['Set-Cookie']);   //保存Cookie到Storage
        this.setData({
          user:{
            name:res.data.data.name,
          }
        })
      }
    }else{
      wx.redirectTo({
        url: '../login/index',
      })
    }
  }
})