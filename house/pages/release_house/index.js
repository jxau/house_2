import {request} from "../../request/index.js";

Page({
  data: {
    houseList:[]
  },
  onLoad: function (options) {
    if(wx.getStorageSync("cookieKey")==null||wx.getStorageSync("cookieKey")==""){
      wx.reLaunch({
        url: '../login/index',
      });
      return;
    }
    this.geHouseList();
  },
  // 获取房源列表数据
  async geHouseList(){
    //创建header
    let header;
    header = {
      'content-type': 'application/x-www-form-urlencoded',
      'cookie':wx.getStorageSync("cookieKey")//读取cookie
    };
    const res=await request({url:"/user/house/myHouse", method: "POST", header: header});
    const {code} = res.data;
    if (res && res.header && res.header['Set-Cookie']) {
      wx.setStorageSync('cookieKey', res.header['Set-Cookie']);   //保存Cookie到Storage
    }
    if(code===200){
      this.setData({
        // 拼接了数组
        houseList:[...res.data.data]
      })
    }else{
      wx.reLaunch({
        url: '../login/index',
      })
    }
  }
})