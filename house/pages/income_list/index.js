import {request} from "../../request/index.js";

Page({
  data: {
    orderList:[]
  },

  onLoad: function (options) {
    if(wx.getStorageSync("cookieKey")==null||wx.getStorageSync("cookieKey")==""){
      wx.reLaunch({
        url: '../login/index',
      });
      return;
    }
    this.loadOrder();
  },
  async loadOrder(){
    //创建header
    let header;
    header = {
      'content-type': 'application/x-www-form-urlencoded',
      'cookie':wx.getStorageSync("cookieKey")//读取cookie
    };
    const res = await request({ url: "/user/order/allOrderBySaid", method: "POST" ,header: header });
    if (res && res.header && res.header['Set-Cookie']) {
      wx.setStorageSync('cookieKey', res.header['Set-Cookie']);   //保存Cookie到Storage
    }
    const {code} = res.data;
    if(code===200){
      this.setData({
        orderList:res.data.data
      })
    }else{
      wx.reLaunch({
        url: '../login/index',
      })
    }
  }
})